#PTR labo 3
P-B Monaco

##Introduction
Le but de ce laboratoire est de se familiariser avec l'API Xenomai et d'observer les différences avec d'autres méthodes.

##Etape 1
Test du fichier timeout du labo 2.  
Voici le résumé du summary, sans et avec stress :
	
```
	Total of 70 values (in msec)

			Solo			Stressed  
Minimum		278				24  
Maximum		10234			12017063  
Sum			69969694		134127068  
Mean		999.567057 		1916.100971  
Variance	1829.750160 	6869986877.616203  
Std Dev		42.775579 		82885.383981  
CoV			0.042794 		43.257315  
```  

On voit clairement l'influence de dohell dans les valeurs obtenues :
 - La somme est bien plus grande.
 - La moyenne est preque 2x plus grande,le programme est donc en moyenne en retard de 900 msec.
 - La variance est disproportionnée pour le range des données.
 - L'écart type et surtout la covariance sont si grandes qu'il n'est pas utile ou significatif de faire des périodes d'une seconde.

PS : Il est possible que quelque chose se soit mal passé car les valeurs sont vraiment extrêmes ici.

##Etape 2
Les threads POSIX et les priorités.  
Le fichier [timeout.c](code/etape2/timeout.c) a été réécrit en faisant l'échantillonage dans une thread POSIX.

Voici le résumé du summary, sans et avec stress :
```
								Total of 70 values

				timeout 1 (msec)						timeout 2 (nsec)
			Solo			Stressed			Solo			Stressed
Minimum		278				24					1000013813		1000011194
Maximum		10234			12017063			1000066580		1000054186
Sum			69969694		134127068			70001266424		70001105338  
Mean		999.567057 		1916.100971			1000018091 		1000015790  
Variance	1829.750160 	6869986877.616203	49355392 		40199296  	
Std Dev		42.775579 		82885.383981		7025.339280  	6340.291476  
CoV			0.042794 		43.257315			0.000007 		0.000006  
```
Malgré la différence d'unité, la covariance indique clairement que timeout 2 est plus précis.
Un élément intéressant est que le timeout 1 dans un environnement stressé indique de plus grands écarts-types que timeout 2.
L'écart type n'étant pas pondéré, on observe ici que les variations sont nettement plus petites avec POSIX.  
Le timeout 2 donne des valeurs étonnantes lorsqu'il s'agit de la différence entre l'environnement solo ou stressé.
Malgré plusieurs tests effectués sur différentes machines, il semblerai que ce code soit insensible au dohell...
Gageons alors que les paramètres de priorités ayant été settés au max (du max!) garantissent une priorité totale dans les environnements stressés.
La différence entre les deux timer ne serait donc plus vraiment significative.

###Code

```c
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <errno.h>

#define MILLI 	1000l
#define MICRO 	1000000l
#define NANO	1000000000l

/*Time structures*/
struct timespec tv;	//Time sampler
struct timespec wv; //Period descriptor

/*Time values [ms]*/
static unsigned long long duration = MILLI; //Duration
static unsigned long long sleep = MILLI;	//Period

/*Sampling thread*/
pthread_t my_thread;


/*
 * Simple task witch runs time sample every 'sleep'[ms] until 'duration'[ms] is reached
*/
void* periodic_task (void* cookie)
{
	unsigned long long sum = 0;
	suseconds_t old_time = 0;

	/*Sampling realtime*/
	clock_gettime(CLOCK_REALTIME,&tv);

	/*While duration is no over (The sum is in nsec but duration in msec so we need to multiply duration by 1E6 */
	while(sum < (duration*MICRO))
	{
		/*Getting last values in old_time*/
		old_time = tv.tv_nsec + tv.tv_sec*NANO;
		/*The thread will wait 'wv'[sec + nanosec]*/
		nanosleep(&wv,NULL);
		/*Sampling realtime*/
		clock_gettime(CLOCK_REALTIME,&tv);
		/*Getting the difference between two samples*/
		long long diff_time = (tv.tv_nsec + tv.tv_sec*NANO - old_time);
		/*Summing the difference to detect end of measurements*/
		sum += diff_time;
		/*Outputting difference result*/
		printf ("%lld\n",diff_time);
	}
}

int main (int argc, char **argv)
{
	/*Getting args and parsing them*/
	if(argc > 2)
	{
		duration = atoi(argv[1]);
		sleep = atoi(argv[2]);
	}

	/*Setting the period time structure*/
	wv.tv_sec = sleep/MILLI;
	wv.tv_nsec = (sleep%MILLI)*MILLI;

	/*Creating a thread from periodic_task() function*/
	pthread_create(&my_thread, NULL, &periodic_task, NULL);

	//Thanks yonch at http://www.yonch.com/tech/82-linux-thread-priority

	// struct sched_param is used to store the scheduling priority
	struct sched_param params;

	// We'll set the priority to the maximum.
	params.sched_priority = sched_get_priority_max(SCHED_FIFO);

	// Attempt to set thread real-time priority to the SCHED_FIFO policy
	int ret = pthread_setschedparam(my_thread, SCHED_FIFO, &params);
	if (ret != 0) {
	 // Print the error
	 printf("Unsuccessful in setting thread realtime prio, error %d\n",ret);
	 return;
	}
	//END Thanks yonch

    //Waiting thread before quitting main
    pthread_join(my_thread, NULL);
}

```

##Etape 3
Taches et fonctions Xenomai.  
Le fichier [xenomai_timer.c](code/etape3/xenomai_timer.c) a été créé sur la base de timeout 2.
Il s'agit donc de lancer le mesures dans une thread séparée, une rt_task.
La différence avec la posix thread qui attend un certain temps, les rt task périodiques garantissent un régularité plus stricte.
Notemment à l'aide de la fonction rt_task_wait_period() qui permet de mettre la task en attente avant la prochaine période. 
Les résultats suivant indiquent que son fonctionnement est beaucoup plus précis.
```
				xenomai (nsec)						timeout 2 (nsec)
			Solo			Stressed			Solo			Stressed  
Minimum		999993302		999990812			1000013813		1000011194  
Maximum		1000005422		1000013780			1000066580		1000054186  
Sum			70000002179		70000015178			70001266424		70001105338  
Mean		1000000031 		1000000216			1000018091 		1000015790  
Variance	8159744 		9719552				49355392 		40199296  	
Std Dev		2856.526562		3117.619605			7025.339280  	6340.291476  
CoV			0.000003 		0.000003			0.000007 		0.000006  
```
Encore une fois, les résultats montrent que les deux technologies sont resistantes dans un environnement difficile mais la rt thread à l'avantage d'étre plus régulière, d'avoir moins de jigue (Covariance).
L'API de Xenomai offre une multitude de fonctions pour pouvoir maitriser les conditions d'exécution de nos programmes.
Les valeurs obtenues sont si proches que je n'ai pas réussit à faire un histogramme utile :  
![exemplehisto](doc/exemplehisto.png)
J'ai ensuite utilisé le data analysis tool de excel. Voici les données graphiqeus :  
![diffposix](doc/diffposix.png)  
![diffrt](doc/diffrt.png)  
Malgré la faible quantité de données, il est possible d'observer une granularité légérement plus grossière pour POSIX, avec un pic à 1500 nsec.
La version xenomai est constante entre 500 et 2500 et beaucoup de valeurs ont du être ignorée de l'histogramme posix.

###Code
```c
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <rtdk.h>
#include <sys/mman.h>
#include <native/task.h>
#include <native/timer.h>

#define TASK_NAME "MA TASK"

#define MILLI 	1000l
#define MICRO 	1000000l
#define NANO	1000000000l

/*Time values [ms]*/
static unsigned long long duration = MILLI; //Duration
static unsigned long long sleep = MILLI;	//Period

/*The periodic sampling thread*/
RT_TASK my_thread;

/*
 * Function wrapper that prints error infos and exit in case of error
 */
void fail_test(int ret, char* fnt_name)
{
	if(ret)
	{
		rt_printf("Call to %s failed with error(%u) : %s\n",fnt_name,ret,strerror(ret));
		exit(ret);
	}
}

/*
 *	Macro that allows easier usage of fail_test function
 */
#define FAILPROOF(_fnt_) fail_test(_fnt_,#_fnt_)

/*
 * Simple task witch runs time samples every 'sleep'[ms] until 'duration'[ms] is reached
*/
void periodic_task(void* cookie)
{
	static unsigned long long sum = 0;
	RTIME old_time;

	/*Setting task as periodic (Sleep is set in msec by user but has to be nsec in the following function)*/
	FAILPROOF(rt_task_set_periodic(NULL, TM_NOW, sleep*MICRO));

	/*Sampling timer*/
	old_time =  rt_timer_read();

	/*While duration is no over (The sum is in nsec but duration in msec so we need to multiply duration by 1E6 */
	while(sum < (duration * MICRO))
	{
		/*The thread will be waken each sleep*MICRO nsec*/
		rt_task_wait_period(NULL);

		/*Sampling timer*/
		RTIME time = rt_timer_read();

		/*Getting the difference between two samples*/
		unsigned long long diff_time = (time - old_time);
		/*Summing the difference to detect end of measurements*/
		sum += diff_time;

		/*Outputting with real-time function*/
		rt_printf ("%lld\n",diff_time);
		/*Setting old value as current one*/
		old_time = time;
	}
}

int main (int argc, char **argv)
{
	/*Preventing current and future pages to be swapped, RT mendatory!*/
	mlockall(MCL_CURRENT|MCL_FUTURE);
	/*Allowing rt_printf function*/
	rt_print_auto_init(1);

	/*Getting args and parsing them*/
	if(argc > 2)
	{
		duration = atoi(argv[1]);
		sleep = atoi(argv[2]);
	}

	/* Creating and starting sampling thread and waiting the end before exiting main*/
	FAILPROOF(rt_task_create(&my_thread, TASK_NAME, 0, 99,  T_FPU|T_CPU(0)|T_JOINABLE));
	FAILPROOF(rt_task_start(&my_thread, periodic_task, NULL));
	FAILPROOF(rt_task_join(&my_thread));
}
```

##Conclusion

Il est a noter que les échantillonages ont été faits bêtements (malheureusement) et avec plus de temps auraient été faits avec au moins 1000 echantillons.  
Le timeout 1 devrait également étre amélioré afin de fournir les mêmes unités que timeout 2 et xenomai.
Paralellement les arguments de timeout 2 et xenomai devraient être modifiés afin de suivre la même logique que timeout 1.



