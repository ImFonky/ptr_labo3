#!/bin/bash

gcc -o timeout timeout.c -pthread;

taskset -pc 0 $$;
mkdir testitest/;
sudo /usr/lib/xenomai/testsuite/dohell -b ../cpu_loop -s 10.192.22.221 -m testitest 70 &
sudo ./timeout 70000 1000 > t1000_70k.dat &&
../summary < t1000_70k.dat > t1000_70k.sum;
