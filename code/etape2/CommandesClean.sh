#!/bin/bash

gcc -o timeout timeout.c -pthread;

taskset -pc 0 $$;
sudo ./timeout 70000 1000 > t1000_70k_clean.dat &&
../summary < t1000_70k_clean.dat > t1000_70k_clean.sum;
