#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>
#include <errno.h>

#define MILLI 	1000l
#define MICRO 	1000000l
#define NANO	1000000000l

/*Time structures*/
struct timespec tv;	//Time sampler
struct timespec wv; //Period descriptor

/*Time values [ms]*/
static unsigned long long duration = MILLI; //Duration
static unsigned long long sleep = MILLI;	//Period

/*Sampling thread*/
pthread_t my_thread;


/*
 * Simple task witch runs time sample every 'sleep'[ms] until 'duration'[ms] is reached
*/
void* periodic_task (void* cookie)
{
	unsigned long long sum = 0;
	suseconds_t old_time = 0;
	
	/*Sampling realtime*/
	clock_gettime(CLOCK_REALTIME,&tv);
	
	/*While duration is no over (The sum is in nsec but duration in msec so we need to multiply duration by 1E6 */
	while(sum < (duration*MICRO))
	{
		/*Getting last values in old_time*/
		old_time = tv.tv_nsec + tv.tv_sec*NANO;
		/*The thread will wait 'wv'[sec + nanosec]*/
		nanosleep(&wv,NULL);
		/*Sampling realtime*/
		clock_gettime(CLOCK_REALTIME,&tv);
		/*Getting the difference between two samples*/
		long long diff_time = (tv.tv_nsec + tv.tv_sec*NANO - old_time);
		/*Summing the difference to detect end of measurements*/
		sum += diff_time;
		/*Outputting difference result*/
		printf ("%lld\n",diff_time);
	}
}

int main (int argc, char **argv)
{
	/*Getting args and parsing them*/
	if(argc > 2)
	{
		duration = atoi(argv[1]);
		sleep = atoi(argv[2]);
	}
		
	/*Setting the period time structure*/
	wv.tv_sec = sleep/MILLI;
	wv.tv_nsec = (sleep%MILLI)*MILLI;
	
	/*Creating a thread from periodic_task() function*/
	pthread_create(&my_thread, NULL, &periodic_task, NULL);
	
	//Thanks yonch at http://www.yonch.com/tech/82-linux-thread-priority
	
	// struct sched_param is used to store the scheduling priority
	struct sched_param params;

	// We'll set the priority to the maximum.
	params.sched_priority = sched_get_priority_max(SCHED_FIFO);

	// Attempt to set thread real-time priority to the SCHED_FIFO policy
	int ret = pthread_setschedparam(my_thread, SCHED_FIFO, &params);
	if (ret != 0) {
	 // Print the error
	 printf("Unsuccessful in setting thread realtime prio, error %d\n",ret);
	 return;     
	}
	//END Thanks yonch
     
    //Waiting thread before quitting main
    pthread_join(my_thread, NULL);
}
