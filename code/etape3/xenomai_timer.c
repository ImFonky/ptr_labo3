#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <rtdk.h>
#include <sys/mman.h>
#include <native/task.h>
#include <native/timer.h>

#define TASK_NAME "MA TASK"

#define MILLI 	1000l
#define MICRO 	1000000l
#define NANO	1000000000l

/*Time values [ms]*/
static unsigned long long duration = MILLI; //Duration
static unsigned long long sleep = MILLI;	//Period

/*The periodic sampling thread*/
RT_TASK my_thread;

/*
 * Function wrapper that prints error infos and exit in case of error
 */
void fail_test(int ret, char* fnt_name)
{
	if(ret)
	{
		rt_printf("Call to %s failed with error(%u) : %s\n",fnt_name,ret,strerror(ret));
		exit(ret);
	}
}

/*
 *	Macro that allows easier usage of fail_test function 
 */
#define FAILPROOF(_fnt_) fail_test(_fnt_,#_fnt_)

/*
 * Simple task witch runs time samples every 'sleep'[ms] until 'duration'[ms] is reached
*/
void periodic_task(void* cookie)
{
	static unsigned long long sum = 0;
	RTIME old_time;
	
	/*Setting task as periodic (Sleep is set in msec by user but has to be nsec in the following function)*/
	FAILPROOF(rt_task_set_periodic(NULL, TM_NOW, sleep*MICRO));

	/*Sampling timer*/
	old_time =  rt_timer_read();
	
	/*While duration is no over (The sum is in nsec but duration in msec so we need to multiply duration by 1E6 */
	while(sum < (duration * MICRO))
	{
		/*The thread will be waken each sleep*MICRO nsec*/
		rt_task_wait_period(NULL);
				
		/*Sampling timer*/
		RTIME time = rt_timer_read();

		/*Getting the difference between two samples*/
		unsigned long long diff_time = (time - old_time);
		/*Summing the difference to detect end of measurements*/
		sum += diff_time;

		/*Outputting with real-time function*/
		rt_printf ("%lld\n",diff_time);
		/*Setting old value as current one*/
		old_time = time;
	}
}

int main (int argc, char **argv)
{
	/*Preventing current and future pages to be swapped, RT mendatory!*/
	mlockall(MCL_CURRENT|MCL_FUTURE);
	/*Allowing rt_printf function*/
	rt_print_auto_init(1);
	
	/*Getting args and parsing them*/
	if(argc > 2)
	{
		duration = atoi(argv[1]);
		sleep = atoi(argv[2]);
	}
	
	/* Creating and starting sampling thread and waiting the end before exiting main*/
	FAILPROOF(rt_task_create(&my_thread, TASK_NAME, 0, 99,  T_FPU|T_CPU(0)|T_JOINABLE));
	FAILPROOF(rt_task_start(&my_thread, periodic_task, NULL));
	FAILPROOF(rt_task_join(&my_thread));
}
